package dwdpollen

import (
	"encoding/json"
	"strings"
	"time"
)

type DWDPollen struct {
	Regions       map[int]*PollenRegion
	LastUpdate    time.Time
	NextUpdate    time.Time
	AllergenTypes []string
}

type Response struct {
	Sender     string    `json:"sender"`
	Regions    []Region  `json:"content"`
	Name       string    `json:"name"`
	LastUpdate time.Time `json:"last_update"`
	NextUpdate time.Time `json:"next_update"`
	Legend     Legend    `json:"legend"`
}

func parseUpdate(rawUpdate json.RawMessage, location *time.Location) (time.Time, error) {
	strUpdate := string(rawUpdate)
	t, err := time.ParseInLocation("2006-01-02 15:04 Uhr", strUpdate[1:len(strUpdate)-1], location)
	if err != nil {
		return t, err
	}

	return t, nil
}

func (r *Response) UnmarshalJSON(data []byte) error {
	var err error
	var v map[string]json.RawMessage
	if err = json.Unmarshal(data, &v); err != nil {
		return err
	}

	r.Sender = string(v["sender"])
	r.Name = string(v["name"])

	loc, _ := time.LoadLocation("Europe/Berlin")

	r.LastUpdate, err = parseUpdate(v["last_update"], loc)
	if err != nil {
		return err
	}
	r.NextUpdate, err = parseUpdate(v["next_update"], loc)
	if err != nil {
		return err
	}

	var legend Legend
	err = json.Unmarshal(v["legend"], &legend)
	if err != nil {
		return err
	}
	r.Legend = legend

	var regions []Region
	err = json.Unmarshal(v["content"], &regions)
	if err != nil {
		return err
	}
	r.Regions = regions

	return nil
}

type Allergen struct {
	Tomorrow   string `json:"tomorrow"`
	Today      string `json:"today"`
	DayafterTo string `json:"dayafter_to"`
}

type Region struct {
	RegionID       int                 `json:"region_id"`
	PartregionName string              `json:"partregion_name"`
	Pollen         map[string]Allergen `json:"Pollen"`
	PartregionID   int                 `json:"partregion_id"`
	RegionName     string              `json:"region_name"`
}

func (r *Region) GetAllergenTypes() []string {
	keys := make([]string, 0, len(r.Pollen))
	for key := range r.Pollen {
		keys = append(keys, key)
	}
	return keys
}

type Legend map[string]string

func (l *Legend) UnmarshalJSON(data []byte) error {
	var err error
	var v map[string]string
	if err = json.Unmarshal(data, &v); err != nil {
		return err
	}

	legend := make(map[string]string)

	for key, value := range v {
		if !strings.HasSuffix(key, "_desc") {
			legend[value] = v[key+"_desc"]
		}
	}

	*l = legend

	return nil
}

type AllergenType string

const (
	Hasel    AllergenType = "Hasel"
	Graeser               = "Graeser"
	Esche                 = "Esche"
	Ambrosia              = "Ambrosia"
	Beifuss               = "Beifuss"
	Roggen                = "Roggen"
	Birke                 = "Birke"
	Erle                  = "Erle"
)

type PollenRegion struct {
	Today            map[AllergenType]string
	Tomorrow         map[AllergenType]string
	DayAfterTomorrow map[AllergenType]string
}
