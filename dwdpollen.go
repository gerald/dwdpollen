package dwdpollen

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"
)

func (d *DWDPollen) loadData() (Response, error) {
	resp, err := http.Get("https://opendata.dwd.de/climate_environment/health/alerts/s31fg.json")
	if err != nil {
		return Response{}, err
	}
	defer resp.Body.Close()

	respBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return Response{}, err
	}

	var jsonResp Response
	err = json.Unmarshal(respBytes, &jsonResp)
	if err != nil {
		return Response{}, err
	}

	return jsonResp, err
}

func (d *DWDPollen) LoadData() error {
	resp, err := d.loadData()
	if err != nil {
		return err
	}

	allergenTypes := resp.Regions[0].GetAllergenTypes()

	d.Regions = make(map[int]*PollenRegion)

	for _, region := range resp.Regions {
		id := region.PartregionID
		if id == -1 {
			id = region.RegionID
		}

		today := make(map[AllergenType]string)
		tomorrow := make(map[AllergenType]string)
		dayAfterTomorrow := make(map[AllergenType]string)

		for _, allergen := range allergenTypes {
			today[AllergenType(allergen)] = resp.Legend[region.Pollen[allergen].Today]
			tomorrow[AllergenType(allergen)] = resp.Legend[region.Pollen[allergen].Tomorrow]
			dayAfterTomorrow[AllergenType(allergen)] = resp.Legend[region.Pollen[allergen].DayafterTo]
		}

		d.Regions[id] = &PollenRegion{
			Today:            today,
			Tomorrow:         tomorrow,
			DayAfterTomorrow: dayAfterTomorrow,
		}
	}

	d.LastUpdate = resp.LastUpdate
	d.NextUpdate = resp.NextUpdate

	d.AllergenTypes = allergenTypes

	return nil
}

func (d *DWDPollen) GetRegion(regionID int) (*PollenRegion, error) {
	if d.NextUpdate.IsZero() || time.Now().After(d.NextUpdate) {
		err := d.LoadData()
		if err != nil {
			return nil, err
		}
	}
	if r, ok := d.Regions[regionID]; ok {
		return r, nil
	}

	return nil, fmt.Errorf("unknown region %d", regionID)
}

func (d *DWDPollen) Summarize(forecast map[AllergenType]string) string {
	var sb strings.Builder

	sb.WriteString("Pollenflugvorhersage:\n\n")

	empty := true

	for _, allergen := range d.AllergenTypes {
		at := AllergenType(allergen)
		f := forecast[at]
		if f != "" && f != "keine Belastung" {
			fmt.Fprintf(&sb, "%s: %s\n", allergen, f)
			empty = false
		}
	}

	res := sb.String()

	if empty {
		return res + "keine"
	}

	return res
}
